package app;

public class EntidadPersona {

  private String clave;
  private String nombre;
  private String domicilio;
  private String telefono;
  private String mail;
  private String fecha;
  private String genero;

  //private Collection (otra entity);
  public String getClave() {
    return clave;
  }

  public void setClave(String value) {
    clave = value;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String value) {
    nombre = value;
  }

  public String getDomicilio() {
    return domicilio;
  }

  public void setDomicilio(String value) {
    domicilio = value;
  }

  public String getTelefono() {
    return telefono;
  }

  public void setTelefono(String value) {
    telefono = value;
  }

  public String getEmail() {
    return mail;
  }

  public void setEmail(String value) {
    mail = value;
  }

  public String getFecha() {
    return fecha;
  }

  public void setFecha(String value) {
    fecha = value;
  }

  public String getGenero() {
    return genero;
  }

  public void setGenero(String value) {
    genero = value;
  }
}
