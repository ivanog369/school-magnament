package app;

import static app.LimitePersona.cbxGenero;
import static app.LimitePersona.txtClave;
import static app.LimitePersona.txtDomicilio;
import static app.LimitePersona.txtEmail;
import static app.LimitePersona.txtId;

import static app.LimitePersona.txtNombre;
import static app.LimitePersona.txtTelefono;
import java.awt.HeadlessException;
import dbUtil.dbConnection;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Calendar;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class ControlCRUDPersona {

  TableRowSorter nameFilter;
  ResultSetMetaData rsmd;

  Connection con;
  PreparedStatement ps;
  ResultSet rs;
  EntidadPersona entPersona = new EntidadPersona();
  String sql;

  Date date;
  Calendar calendar;

  public void alta() {
    try {
      setEntidad();
      con = dbConnection.getConnection();
      sql = "INSERT INTO persona (clave_persona, nombre, domicilio, telefono, "
        + "correo_electronico, fecha_nacimiento, genero) VALUES(?,?,?,?,?,?,?) ";
      ps = con.prepareStatement(sql);
      ps.setString(1, entPersona.getClave());
      ps.setString(2, entPersona.getNombre());
      ps.setString(3, entPersona.getDomicilio());
      ps.setString(4, entPersona.getTelefono());
      ps.setString(5, entPersona.getEmail());
      ps.setString(6, entPersona.getFecha());
      ps.setString(7, entPersona.getGenero());

      int res = ps.executeUpdate();

      if (res > 0) {
        JOptionPane.showMessageDialog(null, "Persona Guardada");
        limpiarCajas();
      } else {
        JOptionPane.showMessageDialog(null, "Error al Guardar persona");
        limpiarCajas();
      }

      con.close();

    } catch (HeadlessException | SQLException e) {
      System.err.println(e);
    }
  }

  public void buscar_persona() {
    if (LimitePersona.tablaAlumnos.getRowCount() != 0) {
      LimitePersona.txtBuscar.addKeyListener(new KeyAdapter() {
        @Override
        public void keyReleased(final KeyEvent e) {
          int colToFind = 1;
          if (LimitePersona.selecBuscarPor.getSelectedItem() == "Nombre") {
            colToFind++;
          }
          if (LimitePersona.selecBuscarPor.getSelectedItem() == "Telefono") {
            colToFind = 4;
          }
          nameFilter.setRowFilter(RowFilter.regexFilter(LimitePersona.txtBuscar.getText(), colToFind));
        }
      });
      nameFilter = new TableRowSorter(LimitePersona.tablaAlumnos.getModel());
      LimitePersona.tablaAlumnos.setRowSorter(nameFilter);
    }
  }

  public void actualiza() {
    try {
      setEntidad();
      con = dbConnection.getConnection();
      sql = "UPDATE persona SET clave_persona=?, nombre=?, domicilio=?, telefono=?, "
        + "correo_electronico=?, fecha_nacimiento=?, genero=? WHERE ID_persona=?";
      ps = con.prepareStatement(sql);
      ps.setString(1, entPersona.getClave());
      ps.setString(2, entPersona.getNombre());
      ps.setString(3, entPersona.getDomicilio());
      ps.setString(4, entPersona.getTelefono());
      ps.setString(5, entPersona.getEmail());
      ps.setString(6, entPersona.getFecha());
      ps.setString(7, entPersona.getGenero());
      ps.setInt(8, Integer.parseInt(LimitePersona.txtId.getText()));

      int res = ps.executeUpdate();

      if (res > 0) {
        JOptionPane.showMessageDialog(null, "Persona Modificada");
        limpiarCajas();
      } else {
        JOptionPane.showMessageDialog(null, "Error al Modificar persona");
        limpiarCajas();
      }

      con.close();

    } catch (HeadlessException | SQLException e) {
      System.err.println(e);
    }
  }

  public void borra() {
    try {
      setEntidad();
      con = dbConnection.getConnection();
      sql = "DELETE FROM persona WHERE ID_persona=?";
      ps = con.prepareStatement(sql);
      ps.setInt(1, Integer.parseInt(LimitePersona.txtId.getText()));

      int res = ps.executeUpdate();

      if (res > 0) {
        JOptionPane.showMessageDialog(null, "Persona Eliminada");
        limpiarCajas();
      } else {
        JOptionPane.showMessageDialog(null, "Error al eliminar persona");
        limpiarCajas();
      }

      con.close();

    } catch (HeadlessException | SQLException e) {
      System.err.println(e);
    }
  }

  public void cargar_personas() {
    if (!"".equals(LimitePersona.txtBuscar.getText())) {
      JOptionPane.showMessageDialog(null, "Borrar el contenido del campo buscar.");
      return;
    }

    try {
      con = (Connection) dbConnection.getConnection();
      sql = "SELECT * FROM `persona`";
      ps = con.prepareStatement(sql);
      rs = ps.executeQuery();
      rsmd = rs.getMetaData();
      int colSize = rsmd.getColumnCount();

      Utilidades.limpiarTabla(LimitePersona.tablaAlumnos);
      while (rs.next()) {
        Object[] row = new Object[colSize];

        for (short elem = 0; elem < colSize; elem++) {
          row[elem] = rs.getObject(elem + 1);
        }

        ((DefaultTableModel) LimitePersona.tablaAlumnos.getModel()).addRow(row);
      }

    } catch (HeadlessException | SQLException ex) {
      System.err.println(ex);
    }
  }

  public void limpiarCajas() {
    txtId.setText("");
    txtClave.setText("");
    txtNombre.setText("");
    txtDomicilio.setText("");
    txtTelefono.setText("");
    txtEmail.setText("");
    cbxGenero.setSelectedIndex(0);
    LimitePersona.jdcFechaNacimiento.setDate(null);
  }

  private void setEntidad() {
    entPersona.setClave(txtClave.getText());
    entPersona.setNombre(txtNombre.getText());
    entPersona.setDomicilio(txtDomicilio.getText());
    entPersona.setTelefono(txtTelefono.getText());
    entPersona.setEmail(txtEmail.getText());
    entPersona.setGenero(cbxGenero.getSelectedItem().toString());
    entPersona.setFecha(Utilidades.date_to_string(LimitePersona.jdcFechaNacimiento.getDate()));
  }

  public void tabla_a_campos_personas() {
    int rec = LimitePersona.tablaAlumnos.getSelectedRow();
    LimitePersona.txtId.setText(LimitePersona.tablaAlumnos.getValueAt(rec, 0).toString());
    LimitePersona.txtClave.setText(LimitePersona.tablaAlumnos.getValueAt(rec, 1).toString());
    LimitePersona.txtNombre.setText(LimitePersona.tablaAlumnos.getValueAt(rec, 2).toString());
    LimitePersona.txtDomicilio.setText(LimitePersona.tablaAlumnos.getValueAt(rec, 3).toString());
    LimitePersona.txtTelefono.setText(LimitePersona.tablaAlumnos.getValueAt(rec, 4).toString());
    LimitePersona.txtEmail.setText(LimitePersona.tablaAlumnos.getValueAt(rec, 5).toString());
    LimitePersona.jdcFechaNacimiento.setDate(Utilidades.string_to_date((String) LimitePersona.tablaAlumnos.getValueAt(rec, 6)));
    LimitePersona.cbxGenero.setSelectedItem(LimitePersona.tablaAlumnos.getValueAt(rec, 7).toString());
  }
}
