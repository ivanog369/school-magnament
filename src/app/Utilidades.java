package app;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class Utilidades {

  public static java.util.Date string_to_date(String fechaString) {
    SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
    try {
      return formatDate.parse(fechaString);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static String date_to_string(java.util.Date fechaDate) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    String sDate = dateFormat.format(fechaDate);
    return sDate;
  }

  public static void limpiarTabla(JTable tabla) {
    try {
      DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();
      int filas = tabla.getRowCount();
      for (int i = 0; filas > i; i++) {
        modelo.removeRow(0);
      }
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, "Error al limpiar la tabla.");
    }
  }
}
