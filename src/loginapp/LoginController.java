package loginapp;

import app.PanelPrincipal;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable{
  
  private LoginModel loginModel = new LoginModel();
  
  @FXML
  private Label dbStatus;
  @FXML
  private TextField usernameField;
  @FXML
  private PasswordField passwdField;
  @FXML
  private Button siginButton;
  @FXML
  private Label logStatus;
  @FXML
  private Button btnClose;
  
  public void initialize(URL url, ResourceBundle rb){
    if (this.loginModel.isDatabaseConnected())
      this.dbStatus.setText("conectado a la Base de Datos");
    else
      this.dbStatus.setText("no se pudo conectar a la Base de Datos");
  }
  
  private void appLogin(){
    PanelPrincipal panelPrincipal = new PanelPrincipal();
    panelPrincipal.setVisible(true);
  }
  
  @FXML
  public void closeLayout(ActionEvent e){
    Stage stage = (Stage) this.btnClose.getScene().getWindow();
    stage.close();
  }
  
  @FXML
  public void Login(ActionEvent event){
    try {
      if (this.loginModel.isLogin(this.usernameField.getText(), this.passwdField.getText())){
        Stage stage = (Stage)this.siginButton.getScene().getWindow();
        stage.close();
        this.appLogin();
      } else
          this.logStatus.setText("Usuario y/o Contraseña incorrectos");
      
    }catch (Exception ex){
      ex.printStackTrace();
    }
  }
}
